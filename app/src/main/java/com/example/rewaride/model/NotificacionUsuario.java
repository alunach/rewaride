package com.example.rewaride.model;

public class NotificacionUsuario {

    public String titulo;
    public String descripcion;

    public NotificacionUsuario( String titulo, String descripcion){
        this.titulo = titulo;
        this.descripcion = descripcion;
    }
}
