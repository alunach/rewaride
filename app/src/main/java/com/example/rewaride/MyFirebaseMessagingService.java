package com.example.rewaride;

import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.rewaride.R;

import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        shownotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        //Log.e("FIREBASE", );
    }

    public void shownotification(String titulo, String descripcion){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"MyNotifications")
                .setContentTitle(titulo)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .setContentText(descripcion);

        NotificationManagerCompat manager =  NotificationManagerCompat.from(this);
        manager.notify(999,builder.build());



    }
    public void onTokenRefresh() {
        Task<String> refreshedToken = FirebaseMessaging.getInstance().getToken();
        Log.i("TAG", "InstanceID: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }
    private void sendRegistrationToServer(Task<String> token) {
        // TODO: Implement this method to send token to your app server.
    }



}

