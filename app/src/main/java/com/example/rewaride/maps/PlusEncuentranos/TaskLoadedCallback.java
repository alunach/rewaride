package com.example.rewaride.maps.PlusEncuentranos;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
